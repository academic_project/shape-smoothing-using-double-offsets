# 3D Smoothing

Shape Smoothing using Double Offsets
Régularisation de formes 3D par dilatation/érosion

L’objectif de ce projet consiste à concevoir un algorithme d’évaluation d’une fonction implicite du double offset, à partir d’un maillage surfacique triangulaire.

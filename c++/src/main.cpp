// Offset
#include <fstream>
#include <iostream>
#include <CGAL/IO/Polyhedron_iostream.h>

#include "types.h"
#include "function.h"

typedef CFunction<Kernel> Function;

void usage()
{
	std::cout << "Usage: mesh input isovalue angle sizing approximation output" << std::endl;
	std::cout << "input: OFF file" << std::endl;
	std::cout << "isovalue: first offset" << std::endl;
	std::cout << "angle: min angle of facets, typically 20 degrees" << std::endl;
	std::cout << "sizing: max sizing of facet elements" << std::endl;
	std::cout << "approximation: approximation error" << std::endl;
	std::cout << "output: MESH file" << std::endl;
}

int main(int argc, char** argv)
{
    std::cout << "OFFSET SURFACES" << std::endl;

	if (argc < 7)
	{ 
		usage();
		return 1;
	}

	double isovalue, angle, sizing, approximation;
	std::sscanf(argv[2], "%lf", &isovalue); // inutile pour l'instant
	std::sscanf(argv[3], "%lf", &angle);
	std::sscanf(argv[4], "%lf", &sizing);
	std::sscanf(argv[5], "%lf", &approximation);
	std::cout << "isovalue: " << isovalue << std::endl;
	std::cout << "angle: " << angle << std::endl;
	std::cout << "sizing: " << sizing << std::endl;
	std::cout << "approximation: " << approximation << std::endl;

	// reads input mesh
	Mesh mesh;
	std::cout << "reading mesh...";
	std::ifstream file(argv[1]);
	file >> mesh;
	std::cout << "done (" << mesh.size_of_facets() << " facets)" << std::endl;

	// Compute bounding sphere
	Bbox ic = CGAL::bbox_3(mesh.points_begin(), mesh.points_end());
	Point center = CGAL::midpoint(Point(ic.min(0), ic.min(1), ic.min(2)), Point(ic.max(0), ic.max(1), ic.max(2)));
    FT radius = std::sqrt(CGAL::squared_distance(center, Point(ic.max(0), ic.max(1), ic.max(2))));
	radius = (radius + std::abs(isovalue)) * 1.2; // a loose bounding box

	Sphere bounding_sphere(center, radius * radius); 

	// single offset implicit function
	Function single_offset(mesh, isovalue, center); 
	Mesh_domain domain = Mesh_domain::create_implicit_mesh_domain(single_offset, bounding_sphere);

	// set mesh criteria
	Mesh_criteria criteria(	CGAL::parameters::facet_angle = angle, // min angle of facets, typically 20 degrees
							CGAL::parameters::facet_size = sizing, // max sizing of facet elements
							CGAL::parameters::facet_distance = approximation, // approximation error
							CGAL::parameters::cell_radius_edge = 2.0, // use default
							CGAL::parameters::cell_size = sizing); // max sizing of tetrahedron elements

	// Mesh generation
	std::cout << "meshing...";
	C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria);
	std::cout << "done" << std::endl;

	// saves to file
	std::cout << "saving to medit file " << argv[6] << "...";
	std::ofstream out(argv[6]);
	c3t3.output_to_medit(out);
	out.close();
	std::cout << "done" << std::endl;

	return EXIT_SUCCESS;
}


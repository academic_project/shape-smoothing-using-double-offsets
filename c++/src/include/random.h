#ifndef _RANDOM_
#define _RANDOM_ 1

inline
double random_double(const double min, const double max)
{
	double range = max - min;
	return min + (double(rand()) / double(RAND_MAX)) * range;
}

inline
double random_double_strictly_inside(const double min, const double max)
{
	double r;
	do
		r = random_double(min,max);
	while(r <= min || r >= max);
	return r;
}


inline
int random_int(const int min, const int max)
{
    int range = max - min;
    return min + int((double(rand())/double(RAND_MAX)) * range);
}

template <class Vector>
Vector random_vec(const double scale, int dim)
{
    const double dx = random_double(-scale, scale);
	const double dy = random_double(-scale, scale);
    if(2 == dim)
		return Vector(dx, dy, 0.0);
    double dz = random_double(-scale, scale);
    return Vector(dx, dy, dz);
}

template <class Vector>
Vector random_vec_partial(const double scale)
{
    double dx = random_double(-scale, scale);
    double dy = random_double(-scale, scale);
    return Vector(dx, dy, 0.0);
}


#endif

#ifndef _FUNCTION_
#define _FUNCTION_


#include <CGAL/basic.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>

#include <memory>

#include "types.h"
#include "random.h"

template <typename Kernel>
class CFunction
{
private:
	typedef CGAL::Bbox_3 				Bbox;
	typedef typename Kernel::Ray_3 		Ray;
	typedef typename Kernel::Segment_3 	Segment;

	// AABB tree 
	typedef typename CGAL::AABB_face_graph_triangle_primitive<Mesh> Primitive;
	typedef typename CGAL::AABB_traits<Kernel, Primitive> Traits;
	typedef typename CGAL::AABB_tree<Traits> Tree;
	typedef typename Tree::Point_and_primitive_id Point_and_primitive_id;

	std::shared_ptr<Tree> 	m_pTree;
	Point					m_center;
	FT 						m_isovalue;

public:
	CFunction(Mesh& mesh, FT isovalue, Point& center)
	{
		m_isovalue = isovalue;
		m_center = center;
		std::cout << "building aabb tree...";
		m_pTree = std::make_shared<Tree>(faces(mesh).first, faces(mesh).second, mesh);
		m_pTree->accelerate_distance_queries();
		std::cout << "done" << std::endl;
	}

	CFunction(const CFunction& func2)
	{
		m_isovalue = func2.get_isovalue();
		m_center = func2.get_center();
		std::cout << "building aabb tree...";
		m_pTree = func2.get_tree();
		std::cout << "done" << std::endl;
	}

	FT get_isovalue() const { return m_isovalue; }
	std::shared_ptr<Tree> get_tree() const { return m_pTree; }
	Point get_center() const { return m_center; }

	// returns scalar function, negative if 
	// inside domain, and positive otherwise.
	FT operator()(const Point& query) const
	{
		// distance to mesh
		FT d = std::sqrt(m_pTree->squared_distance(query));

		// sign
		Segment seg(query, m_center);
		const int nint = (int)m_pTree->number_of_intersected_primitives(seg);
		if (nint % 2 == 0) d *= -1.0;

		return d - m_isovalue;
	}
};

#endif // _FUNCTION_

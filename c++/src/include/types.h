
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#define  BOOST_PARAMETER_MAX_ARITY 12
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef Kernel::FT         FT;
typedef Kernel::Point_3    Point;
typedef Kernel::Sphere_3   Sphere;

// 3D mesh generation
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/Labeled_mesh_domain_3.h>

typedef CGAL::Labeled_mesh_domain_3<Kernel> Mesh_domain;
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;

// input surface triangle mesh
#include <CGAL/Polyhedron_3.h>
typedef CGAL::Polyhedron_3<Kernel> Mesh;

#include <CGAL/bounding_box.h>
typedef CGAL::Bbox_3   	Bbox;
